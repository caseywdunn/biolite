#!/usr/bin/env python
import sys
from Bio.SeqIO.QualityIO import FastqGeneralIterator
with open(sys.argv[2], "w") as f:
	for read in FastqGeneralIterator(open(sys.argv[1])):
		print >>f, "@" + read[0]
		print >>f, read[1]
		print >>f, "+"
		print >>f, read[2]

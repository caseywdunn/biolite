# Installation

These are advanced instructions for installing directly from the tarball or for
installing a development version from the git repository. See the Quick Install
section of the README for instructions on how to install a release version
using Anaconda Python.


## Reference List of Dependencies

See the [conda recipe](dev/conda/meta.yaml) for a full list of dependencies and
their versions. All dependencies are available in
[bioconda](http://bioconda.github.io).

## Installing BioLite from the tarball

BioLite uses the standard `distutils` method of installation:

    tar xf biolite-1.0.0.tar.gz
    cd biolite-1.0.0
    python setup.py install

For an alternate install path, specify a prefix with:

    python setup.py install --prefix=...


## Installing BioLite from the git repo

Same as with the tarball, except clone the repo instead of downloading the tarball:

    git clone https://bitbucket.org/caseywdunn/biolite.git
    cd biolite
    python setup.py install

If you edit the code in the repository, or pull a new version, then run the following
to update the installation:

    python setup.py install


## Generating the tarballs from the git repo

Create a source distribution with `distutils`:

    python setup.py sdist

Create a tarball for biolite-tools with GNU `autoconf`:

    cd tools
    ./autogen.sh
    ./configure
    make dist-gzip


## Building the reference manual

First, generate the Installation section of the manual from this INSTALL file using
[pandoc](http://johnmacfarlane.net/pandoc/installing.html):

    pandoc -f markdown -t rst INSTALL.md >doc/install.rst

Install sphinx and the Read the Docs sphinx theme:

    pip install sphinx sphinx_rtd_theme

Then run:

    cd doc
    make html

The reference manual is built in the `_build/html` subdirectory.


## Configuration

BioLite provides a default configuration file inside its python module at:

    (biolite.__path__) + 'config/biolite.cfg'

This file serves as the default configuration for any user on the system. To
override it on a per-user basis, simply copy the file to
`$HOME/.biolite/biolite.cfg` and make any required changes.

You can also override the location of the configuration file with an
environment variable. In `bash`:

    export BIOLITE_CONFIG=/your/path/to/biolite.cfg

or in `csh`:

    setenv BIOLITE_CONFIG /your/path/to/biolite.cfg

Finally, the BIOLITE_RESOURCE environment variable allows you to temporarily
override specific values in the resources section of the configuration. For
instance, if your configuration file is set to 2 threads, but want to test out
a run with 8 threads instead, you could use (in `bash`):

    export BIOLITE_RESOURCES="threads=8"

The value of this variable can be a comma-separated list of `key=value` pairs.


## Installing biolite-tools from the tarball

BioLite includes some C++ programs called biolite-tools. You can install them
automatically with conda, or manually with the instructions provided here.

To compile biolite-tools, you must at a minimum have a C++ compiler that
supports the TR1 standard, e.g.:

* gcc 4.4 (CentOS 6.3)
* gcc 4.8 (Ubuntu 13.04)
* clang 5.1 (OS X 10.9)

biolite-tools uses the standard GNU `autoconf` method of installation:

    tar xf biolite-tools-0.4.0.tar.gz
    cd biolite-tools-0.4.0
    ./configure
    make install

For an alternate install path, specify a prefix with:

    ./configure --prefix=...
    make install


## Installing biolite-tools from the git repo

If you are installing biolite-tools directly from the git repo, you first need
to generate the configure script (which requires that you have `autoconf` and
`automake` installed):

    git clone https://bitbucket.org/caseywdunn/biolite.git
    cd biolite
    cd tools
    ./autogen.sh
    ./configure
    make install

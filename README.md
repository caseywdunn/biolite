BioLite is developed by the [Dunn Lab](http://dunnlab.org) at Yale University.

# End of support
We no longer actively develop biolite. It may still work well for your 
projects, but we are not adding features, updating dependencies, or providing support.

# Quick Install

On 64-bit Linux, BioLite and all its dependencies can be installed from
[bioconda](http://bioconda.github.io). We have primarily tested BioLite on
CentOS 6.8 and Ubuntu 16.04, but in theory it should run on any Linux system
with glibc >= 2.12.

First, you will need to install the
[Anaconda](https://www.continuum.io/anaconda-overview)
distribution of Python. For a minimal install, you can for example install
Miniconda in your home directory with:

    wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
    bash Miniconda2-latest-Linux-x86_64.sh -b
    echo 'export "PATH=$HOME/miniconda2/bin:$PATH"' >>~/.bashrc
    source ~/.bashrc

NOTE: by installing Miniconda with the -b option, you are acknowledging that
you accept the terms of the
[Anaconda EULA](https://docs.continuum.io/anaconda/eula)
from Continuum Analytics.

Once the `conda` command is in your PATH, configure your machine to use the
bioconda channel (moreinformation about this process is available at the
[bioconda](http://bioconda.github.io) site):

    conda config --add channels defaults
    conda config --add channels conda-forge
    conda config --add channels bioconda

BioLite can then be installed with a single command:

    conda install biolite

Alternatively, BioLite can be installed into its
own isolated conda environment with the command:

    conda create -n biolite biolite

Once installed, activate the "biolite" conda environment with:

    source activate biolite

See [INSTALL](https://bitbucket.org/caseywdunn/biolite/src/master/INSTALL.md)
for more detailed instructions, system requirements, and for how to configure
BioLite after installing it.


# Documentation

[biolite.bitbucket.org](http://biolite.bitbucket.org)


# Background

BioLite is a bioinformatics framework written in Python/C++ that automates the
collection and reporting of diagnostics, tracks provenance, and provides
lightweight tools for building out customized analysis pipelines.

BioLite provides generalized components aimed at developers of NGS analyses
workflows. These include:

* a 'catalog' database for pairing metadata with and organizing NGS data files
* a global 'diagnostics' database that establishes provenance for analyses and
  archives analysis results that can be accessed across multiple stages of a
  workflow, or from different workflows
* a modular 'pipeline' framework with Python wrappers and workflows for commonly
  used NGS tools, and logging, profiling, and checkpointing functionality
* a C++ library 'seqio' for efficient I/O of large NGS data files


# Reporting Problems

Please use the [issue tracker](https://bitbucket.org/caseywdunn/biolite/issues)
at Bitbucket to report and problems you have.


# Updating and Uninstalling

BioLite is under active development. This means that new updates are not always
compatible with analyses that have already been run. We make every effort to
avoid changes to the catalog database structure, so there is usually not a need
to re-catalog data when you install a new version. However, you may need to
rerun already completed analyses if you want to generate new reports or use
existing data with new versions of pipelines.

If you installed BioLite with conda using the quick install instructions above,
uninstall with:

    conda env remove biolite

To upgrade a conda installation, use:

    conda install -n biolite biolite

If you installed BioLite with pip, uninstall with:

    pip uninstall biolite

To upgrade with pip, use:

    pip install -U biolite


# Citing

BioLite is still under development, and is an experimental tool that should be
used with care.  Please cite:

Howison M, Sinnott-Armstrong NA, Dunn CW. 2012. [BioLite, a lightweight
bioinformatics framework with automated tracking of diagnostics and
provenance](https://www.usenix.org/conference/tapp12/biolite-lightweight-bioinformatics-framework-automated-tracking-diagnostics-and).
In *Proceedings of the 4th USENIX Workshop on the Theory and Practice of
Provenance (TaPP '12)*, 14-15 June 2012, Boston, MA, USA.

BioLite makes use of many other programs that do much of the heavy lifting of
the analyses. Please be sure to credit these essential components as well.
Check the biolite.cfg file for web links to these programs, where you can find
more information on how to cite them.


# Funding

This software has been developed with support from the following US National
Science Foundation grants:

PSCIC Full Proposal: The iPlant Collaborative: A Cyberinfrastructure-Centered
Community for a New Plant Biology (Award Number 0735191)

Collaborative Research: Resolving old questions in Mollusc phylogenetics with
new EST data and developing general phylogenomic tools (Award Number 0844596)

Infrastructure to Advance Life Sciences in the Ocean State (Award Number
1004057)

The Brown University [Center for Computation and Visualization](http://www.brown.edu/Departments/CCV/) has been instrumental to the development of BioLite.


# License

Copyright (c) 2012-2018. All rights reserved.

BioLite is distributed under the GNU General Public License version
3. For more information, see LICENSE or visit:
[http://www.gnu.org/licenses/gpl.html](http://www.gnu.org/licenses/gpl.html)

BioLite includes source code from the following projects:

* gzstream C++ interface v1.5, which is distributed under the GNU Lesser
  General Public License in `LICENSE.gzstream`
* Bootstrap v2.3.1 CSS style, which is distributed under the Apache License
  v2.0 in `share/bootstrap.min.css`
* jsphylosvg v1.55, which is distributed under the GPL in `LICENSE.jsphylosvg`,
  and which includes Raphael 1.4.3, which is distributed under the MIT license
* D3js v3.1.9, which is distributed under the BSD license in `LICENSE.d3js`
* MersenneTwister C++ class, which distributed under the BSD license in
  `tools/src/MersenneTwister.h`
